# README #

	public static boolean isBetween(int x, int lower, int upper) {
		  return lower <= x && x <= upper;
		}
	
	public static char intensityToAscii(int intensity) {
		
		if(isBetween(intensity, 0, 25)) return INTENSITY_2_ASCII.charAt(0);
		if(isBetween(intensity, 26, 51)) return INTENSITY_2_ASCII.charAt(1);
		if(isBetween(intensity, 51, 76)) return INTENSITY_2_ASCII.charAt(2);
		if(isBetween(intensity, 77, 102)) return INTENSITY_2_ASCII.charAt(3);
		if(isBetween(intensity, 103, 127)) return INTENSITY_2_ASCII.charAt(4);
		if(isBetween(intensity, 128, 153)) return INTENSITY_2_ASCII.charAt(5);
		if(isBetween(intensity, 154, 179)) return INTENSITY_2_ASCII.charAt(6);
		if(isBetween(intensity, 180, 204)) return INTENSITY_2_ASCII.charAt(7);
		if(isBetween(intensity, 205, 230)) return INTENSITY_2_ASCII.charAt(8);
		if(isBetween(intensity, 231, 255)) return INTENSITY_2_ASCII.charAt(9);

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact